#!groovy

@Library('jenkinslib') _     

def mytools = new org.devops.tools()
//String workspace = "/opt/jenkins/workspace"

//Pipeline
pipeline{
//指定运行此流水线的节点
agent { node { label "master"
               //customWorkspace "${workspace}"   //指定运行工作目录（可选）
        
        }
}
    
//全局变量
environment { 
    activeEnv = 'dev'
}


options {
    timeout(time: 1, unit: 'HOURS')
}

//string 字符串类型的参数, 例如:
parameters { 
    string(
        name: 'hostname', 
        defaultValue: 'host', 
        description: '你需要在哪台机器上进行部署?'
    )
    booleanParam(
        name: 'test_skip_flag',
        defaultValue: true,
        description: '你需要在部署之前执行自动化测试么?'
    )
    choice(
        description: '你需要选择哪个模块进行构建?',
        name: 'modulename',
        choices: ['Module1', 'Module2', 'Module3']
    )
    text(
        name: 'release_note', 
        defaultValue: 'Release Note 信息如下所示: \n \
Bug-Fixed: \n \
Feature-Added: ', 
        description: 'Release Note的详细信息是什么?'
    )
}

//booleanParam 布尔参数, 例如:
//parameters { booleanParam(name: 'DEBUG_BUILD', defaultValue: true, description: '') }

//流水线的阶段
stages{

    //阶段1 获取代码
    stage("CheckOut"){
        steps{
            script{
                mytools.PrintMes("获取代码",'green')
                println("获取代码")
                println("${modulename}")
                println("${hostname}")
                println("${test_skip_flag}")
                println("${release_note}")
            }
        }
    }
    stage("Build"){
        input {
            message '选择部署的环境'
            ok 'deploy'
            submitter 'zhangshaohui'
            parameters {
                choice choices: ['prd', 'uat', 'test'], description: '选择部署环境', name: 'DEPLOY_ENV'
           } 
        }
        steps{
            script{
                println("${DEPLOY_ENV}")
                println("运行构建")
            }
        }
    }
    stage('Example') {
        steps {
            script{
                println("Example")
            }       
        }
    }
}
post {
    always{
        script{
            println("流水线结束后，经常做的事情")
        }
    }
        
    success{
        script{
            println("流水线成功后，要做的事情")
        }
        
    }
    failure{
        script{
            println("流水线失败后，要做的事情")
        }
    }
        
    aborted{
        script{
            println("流水线取消后，要做的事情")
        }
        
    }
}
}
